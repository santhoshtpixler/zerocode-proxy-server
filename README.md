# zerocode-proxy-server

[![pipeline status](https://gitlab.com/santhoshtpixler/zerocode-proxy-server/badges/master/pipeline.svg)](https://gitlab.com/santhoshtpixler/zerocode-proxy-server/commits/master) [![License](https://img.shields.io/badge/license-MIT-blue.svg)](https://opensource.org/licenses/MIT)

## Description
A simple http proxy server which captures HTTP requests to generate  [ZEROCODE](https://github.com/authorjapps/zerocode) test scenarios.

---
For usage refer [Wiki](https://gitlab.com/santhoshtpixler/zerocode-proxy-server/wikis/home)
# Happy testing  :panda_face:

## Dependencies
*  jetty-util-9.4.14.v20181114
* jetty-io-9.4.14.v20181114
* jackson-datatype-jdk8-2.9.8
* javax.servlet-api-3.1.0
* jetty-http-9.4.14.v20181114
* commons-lang3-3.5
* snakeyaml-android:1.20
* javafaker-0.16
* automaton-1.11-8
* jackson-annotations-2.9.8
* jackson-databind-2.9.8
* jetty-proxy-9.4.14.v20181114
* jetty-servlet-9.4.14.v20181114
* jetty-client-9.4.14.v20181114
* jetty-security-9.4.14.v20181114
* generex-1.0.2
* jetty-server-9.4.14.v20181114
* jackson-core-2.9.8



## Powerd by ![Jetty](https://www.eclipse.org/jetty/images/jetty-logo-80x22.png)

## Contributers

*  [santhosh](https://gitlab.com/santhoshtpixler)

## Maintained by [santhosh](https://gitlab.com/santhoshtpixler)
