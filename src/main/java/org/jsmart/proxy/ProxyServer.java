package org.jsmart.proxy;

import java.io.File;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;

public class ProxyServer {

	private static final Logger LOGGER = Log.getLogger(ProxyServer.class);

	public static void main(String[] args) throws Exception {
		int port = 9999;
		if (args != null && args.length == 2 && args[0].equals("-p")) {
			try {
				port = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				LOGGER.warn("Invalid port : {} \n Defaulting to port : 9999", args[0]);
			}
		}
		LOGGER.info("Starting ProxyServer in port : {} ...", port);

		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			LOGGER.info("Wait...scenarios are brewing. Hold on..");
			ProxyServer.wrapUp();
			LOGGER.info("Done. Zerocode scenarios are generated. \n Folder name : zerocode \n Happy Testing.");
		}));

		File outputDir = new File("zerocode-workspace");
		System.setProperty("zc.output.dir", outputDir.getAbsolutePath());
		outputDir.mkdirs();
		Server server = new Server(port);
		ServletHandler handler = new ServletHandler();
		server.setHandler(handler);
		ServletHolder holder = new ServletHolder(ProxySevlet.class);
		holder.setInitParameter("maxThreads", "20");
		handler.addServletWithMapping(holder, "/*");
		server.start();

		server.join();

	}

	public static synchronized void wrapUp() {
		File outputDir = FileUtils.getOutputDir();
		File[] scenarioDirs = outputDir.listFiles();
		for (int i = 0; i < scenarioDirs.length; i++) {
			new ScenarioComposer(scenarioDirs[i]).compose();
		}
		outputDir.renameTo(new File("zerocode"));
		new File(outputDir.getParent() + File.separator + "zerocode-workspace").mkdir();

	}
}
