package org.jsmart.proxy;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.jsmart.zerocode.core.domain.ScenarioSpec;
import org.jsmart.zerocode.core.domain.Step;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ScenarioComposer {

	private static ObjectMapper OBJECT_MAPPER = new ObjectMapper();
	private static Logger LOGGER = Log.getLogger(ScenarioComposer.class);

	private File scenarioDir;
	private ScenarioSpec scenarioSpec;

	public ScenarioComposer(File scenarioDirectory) {
		this.setScenarioDir(scenarioDirectory);
	}

	public void compose() {
		List<File> stepFiles = this.listSteps();
		try {
			List<Step> steps = stepFiles.stream().map(this::readStep).collect(Collectors.toList());
			File scenarioPropFile = FileUtils.getScenarioProperties(scenarioDir);
			try (BufferedReader reader = Files.newBufferedReader(scenarioPropFile.toPath())) {
				Properties scenarioProperties = new Properties();
				scenarioProperties.load(reader);
				this.scenarioSpec = new ScenarioSpec(
						Integer.parseInt(scenarioProperties.getProperty(Constants.SCENARIO_LOOP_COUNT, "0")),
						Boolean.parseBoolean(
								scenarioProperties.getProperty(Constants.SCENATIO_IGNORE_FAILURE, "false")),
						scenarioProperties.getProperty(Constants.SCENARIO_NAME), steps);
				this.persist();
				stepFiles.forEach(File::delete);
			}
			scenarioPropFile.delete();
		} catch (IOException | RuntimeException e) {
			LOGGER.warn("Cannot compose scenario : " + scenarioSpec.getScenarioName(), e);
		}

	}

	private void persist() throws IOException {
		File scenarioJSON = FileUtils.getScenarioFile(scenarioDir);
		Files.write(scenarioJSON.toPath(),
				OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsBytes(this.scenarioSpec),
				StandardOpenOption.TRUNCATE_EXISTING);
	}

	private List<File> listSteps() {
		File[] files = scenarioDir.listFiles((file, name) -> {
			return (name.endsWith(".json") && !file.isHidden());
		});

		List<File> sortedList = Arrays.asList(files);
		Collections.sort(sortedList, (f1, f2) -> {
			return Long.compare(f1.lastModified(), f2.lastModified());
		});
		return sortedList;
	}

	private Step readStep(File stepJSON) {
		try {
			return OBJECT_MAPPER.readValue(stepJSON, Step.class);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * @return the scenarioDir
	 */
	public File getScenarioDir() {
		return scenarioDir;
	}

	/**
	 * @param scenarioDir the scenarioDir to set
	 */
	public void setScenarioDir(File scenarioDir) {
		this.scenarioDir = scenarioDir;
	}

	/**
	 * @return the scenarioSpec
	 */
	public ScenarioSpec getScenarioSpec() {
		return scenarioSpec;
	}

	/**
	 * @param scenarioSpec the scenarioSpec to set
	 */
	public void setScenarioSpec(ScenarioSpec scenarioSpec) {
		this.scenarioSpec = scenarioSpec;
	}

}
