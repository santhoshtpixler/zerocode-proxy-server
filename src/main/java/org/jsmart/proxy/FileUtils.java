package org.jsmart.proxy;

import java.io.File;
import java.io.IOException;

public class FileUtils {

	private static final String OUTPUT_DIR = System.getProperty("zc.output.dir");

	public static File getOutputDir() {
		return new File(OUTPUT_DIR);
	}

	public static File getScenarioDir(String scenarioName) throws IOException {
		File dir = new File(getOutputDir() + File.separator + scenarioName);
		boolean flag = dir.mkdirs();
		if (flag) {
			createScenarioProperties(dir);
		}
		return dir;
	}

	public static File getStepFile(String scenarioName, String stepName) throws IOException {
		File scenarioDir = getScenarioDir(scenarioName);
		File stepJSON = new File(scenarioDir + File.separator + stepName + ".json");
		stepJSON.createNewFile();
		return stepJSON;
	}

	public static File createScenarioProperties(File scenarioDir) throws IOException {
		File scenarioProperties = new File(scenarioDir + File.separator + "scenario.properties");
		scenarioProperties.createNewFile();
		return scenarioProperties;
	}

	public static File getScenarioProperties(String scenarioName) throws IOException {
		File scenarioDir = getScenarioDir(scenarioName);
		return new File(scenarioDir + File.separator + "scenario.properties");
	}

	public static File getScenarioProperties(File scenarioDir) throws IOException {
		return new File(scenarioDir + File.separator + "scenario.properties");
	}

	public static File getScenarioFile(File scenarioDir) throws IOException {
		File f = new File(scenarioDir.getPath() + File.separator + scenarioDir.getName() + ".json");
		f.createNewFile();
		return f;
	}
}
