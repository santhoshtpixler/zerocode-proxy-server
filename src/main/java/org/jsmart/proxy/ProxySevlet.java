package org.jsmart.proxy;

import java.io.IOException;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.proxy.ProxyServlet;

public class ProxySevlet extends ProxyServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		if (request.getRequestURI().equals("/zerocode")) {
			ProxyServer.wrapUp();
			response.getWriter().println("Scenarions generated successfully");
			response.setHeader("content-type", "text/plain");
			response.setStatus(200);
		} else {
			super.service(request, response);
		}
	}

	@Override
	protected String rewriteTarget(HttpServletRequest request) {
		return request.getRequestURL().toString();
	}

	@Override
	protected void addProxyHeaders(HttpServletRequest clientRequest, Request proxyRequest) {
		proxyRequest.header(HttpHeader.ACCEPT_ENCODING, null);
		ResponseListner customListner = new ResponseListner();
		proxyRequest.onResponseHeaders(customListner);
		proxyRequest.onResponseContent(customListner);
		proxyRequest.onComplete(customListner);
		proxyRequest.listener(customListner.getRequestListner());

		Map<String, String[]> paramsMap = clientRequest.getParameterMap();

		paramsMap.entrySet().stream().filter(i -> {
			return !(i.getKey().startsWith("zc."));
		}).forEach(i -> {
			String[] values = i.getValue();
			for (int j = 0; j < values.length; j++) {
				proxyRequest.param(i.getKey(), values[j]);
			}
		});
		Map<String, String> zcAttribute = paramsMap.entrySet().stream().filter(i -> {
			return (i.getKey().startsWith("zc."));
		}).collect(Collectors.toMap(Map.Entry::getKey, v -> {
			return v.getValue()[0];
		}));

		proxyRequest.attribute(Constants.ZC_PARAMS, zcAttribute);
	}

}
