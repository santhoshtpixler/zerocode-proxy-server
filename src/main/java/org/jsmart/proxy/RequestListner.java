package org.jsmart.proxy;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.Locale;

import javax.servlet.MultipartConfigElement;

import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Request.Listener.Adapter;
import org.eclipse.jetty.http.HttpFields;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.http.MultiPartFormInputStream;
import org.eclipse.jetty.util.BufferUtil;

public class RequestListner extends Adapter {

	private final int maxLength;
	private ByteBuffer buffer;
	private String mediaType;
	private String encoding;
	private MultiPartFormInputStream multiPartFormInputStream;
	private boolean multiPart = false;
	private boolean jsonBody = false;
	private boolean binaryBody = false;

	public RequestListner() {
		this(2 * 1024 * 1024);
	}

	public RequestListner(int maxLength) {
		if (maxLength < 0)
			throw new IllegalArgumentException("Invalid max length " + maxLength);
		this.maxLength = maxLength;
	}

	@Override
	public void onHeaders(Request request) {

		HttpFields headers = request.getHeaders();
		long length = headers.getLongField(HttpHeader.CONTENT_LENGTH.asString());
		if (HttpMethod.HEAD.is(request.getMethod()))
			length = 0;
		if (length > maxLength) {
			request.abort(new IllegalArgumentException("Buffering capacity " + maxLength + " exceeded"));
			return;
		}

		String contentType = headers.get(HttpHeader.CONTENT_TYPE);
		if (contentType != null) {
			String media = contentType;

			String charset = "charset=";
			int index = contentType.toLowerCase(Locale.ENGLISH).indexOf(charset);
			if (index > 0) {
				media = contentType.substring(0, index);
				String encoding = contentType.substring(index + charset.length());
				// Sometimes charsets arrive with an ending semicolon.
				int semicolon = encoding.indexOf(';');
				if (semicolon > 0)
					encoding = encoding.substring(0, semicolon).trim();
				// Sometimes charsets are quoted.
				int lastIndex = encoding.length() - 1;
				if (encoding.charAt(0) == '"' && encoding.charAt(lastIndex) == '"')
					encoding = encoding.substring(1, lastIndex).trim();
				this.encoding = encoding;
			}

			int semicolon = media.indexOf(';');
			if (semicolon > 0)
				media = media.substring(0, semicolon).trim();
			this.mediaType = media;
		}

	}

	@Override
	public void onContent(Request response, ByteBuffer content) {
		int length = content.remaining();
		if (length > BufferUtil.space(buffer)) {
			int requiredCapacity = buffer == null ? length : buffer.capacity() + length;
			if (requiredCapacity > maxLength)
				response.abort(new IllegalArgumentException("Buffering capacity " + maxLength + " exceeded"));
			int newCapacity = Math.min(Integer.highestOneBit(requiredCapacity) << 1, maxLength);
			buffer = BufferUtil.ensureCapacity(buffer, newCapacity);
		}
		BufferUtil.append(buffer, content);
	}

	@Override
	public void onSuccess(Request request) {
		String contentType = request.getHeaders().get(HttpHeader.CONTENT_TYPE);
		if (contentType == null) {
			return;
		}
		if (contentType.startsWith("multipart/form-data")) {
			MultipartConfigElement multipartConfigElement = new MultipartConfigElement("");
			setMultiPartFormInputStream(new MultiPartFormInputStream(this.getContentAsInputStream(), contentType,
					multipartConfigElement, null));
			multiPart = true;
		} else if (mediaType.equals("application/json")) {
			jsonBody = true;
		} else {
			binaryBody = true;
		}
	}

	/**
	 * @return the content as a string, using the "Content-Type" header to detect
	 *         the encoding or defaulting to UTF-8 if the encoding could not be
	 *         detected.
	 * @see #getContentAsString(String)
	 */
	public String getContentAsString() {
		String encoding = this.encoding;
		if (encoding == null)
			return getContentAsString(StandardCharsets.UTF_8);
		return getContentAsString(encoding);
	}

	/**
	 * @param encoding the encoding of the content bytes
	 * @return the content as a string, with the specified encoding
	 * @see #getContentAsString()
	 */
	public String getContentAsString(String encoding) {
		if (buffer == null)
			return null;
		return BufferUtil.toString(buffer, Charset.forName(encoding));
	}

	/**
	 * @param encoding the encoding of the content bytes
	 * @return the content as a string, with the specified encoding
	 * @see #getContentAsString()
	 */
	public String getContentAsString(Charset encoding) {
		if (buffer == null)
			return null;
		return BufferUtil.toString(buffer, encoding);
	}

	/**
	 * @return Content as InputStream
	 */
	public InputStream getContentAsInputStream() {
		if (buffer == null)
			return new ByteArrayInputStream(new byte[0]);
		return new ByteArrayInputStream(buffer.array(), buffer.arrayOffset(), buffer.remaining());
	}

	public boolean isMultiPart() {
		return this.multiPart;
	}

	public boolean isJSONBody() {
		return this.jsonBody;
	}

	public boolean isBinaryBody() {
		return this.binaryBody;
	}

	/**
	 * @return the multiPartFormInputStream
	 */
	public MultiPartFormInputStream getMultiPartFormInputStream() {
		return multiPartFormInputStream;
	}

	private void setMultiPartFormInputStream(MultiPartFormInputStream multiPartFormInputStream) {
		this.multiPartFormInputStream = multiPartFormInputStream;
	}

}
