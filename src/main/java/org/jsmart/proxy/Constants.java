package org.jsmart.proxy;

public class Constants {

	public static final String ZC_PARAMS = "zc.params";
	public static final String SCENARIO_NAME = "zc.sc.name";
	public static final String SCENARIO_LOOP_COUNT = "zc.sc.loop";
	public static final String SCENATIO_IGNORE_FAILURE = "zc.sc.ignoreStepFailures";
	public static final String STEP_NAME = "zc.st.name";
	private Constants() {
		// disable object creation
	}
}
