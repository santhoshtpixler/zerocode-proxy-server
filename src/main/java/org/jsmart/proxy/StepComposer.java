package org.jsmart.proxy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;

import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.http.HttpField;
import org.eclipse.jetty.util.log.Log;
import org.eclipse.jetty.util.log.Logger;
import org.jsmart.zerocode.core.domain.Assertions;
import org.jsmart.zerocode.core.domain.Step;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class StepComposer {

	private static final Logger LOGGER = Log.getLogger(StepComposer.class);

	public static final List<String> excludedHeaders = Collections.unmodifiableList(Arrays.asList("Host"));
	private static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

	private Request request;
	private Response response;
	private ResponseListner responseListner;
	private RequestListner requestListner;
	private Map<String, String> zcParams;

	private StepComposer() {
		// NOOP
	}

	@SuppressWarnings("unchecked")
	public StepComposer(Request req, Response resp, ResponseListner respListner) {
		this.setRequest(req);
		this.setResponse(resp);
		this.setResponseListner(respListner);
		this.setRequestListner(respListner.getRequestListner());
		this.zcParams = (Map<String, String>) req.getAttributes().get("zc.params");
	}

	public void compose() {

		Step step = null;
		try {
			Map<String, Object> paramMap = new HashMap<>(request.getParams().getSize());
			request.getParams().forEach(f -> {
				paramMap.put(f.getName(), f.getValue());
			});
			Map<String, Object> headers = request.getHeaders().stream()
					.collect(Collectors.toMap(HttpField::getName, HttpField::getValue));

			removeHeaders(headers);
			String body = this.requestListner.isJSONBody() ? this.requestListner.getContentAsString() : null;
			org.jsmart.zerocode.core.domain.Request requestJSON = null;
			if (body != null) {
				requestJSON = new org.jsmart.zerocode.core.domain.Request(headers, paramMap,
						OBJECT_MAPPER.readTree(body));
			} else {
				requestJSON = new org.jsmart.zerocode.core.domain.Request(headers, paramMap, null);
			}

			Assertions assertions = null;
			assertions = new Assertions(response.getStatus(),
					OBJECT_MAPPER.readTree(this.responseListner.getContentAsString()));
			step = new Step(0, zcParams.getOrDefault(Constants.STEP_NAME, "unknown"), request.getMethod(),
					request.getPath(), OBJECT_MAPPER.convertValue(requestJSON, JsonNode.class),
					OBJECT_MAPPER.convertValue(assertions, JsonNode.class));
		} catch (IOException e) {
			LOGGER.warn("Cannot compose a step!",e);
			throw new RuntimeException(e);
		}

		persist(step);
		LOGGER.info("Step : " + step.getName() + " captured.");
	}

	private void persist(Step step) {

		String scenarioName = this.zcParams.get(Constants.SCENARIO_NAME);
		try {
			Files.write(FileUtils.getStepFile(scenarioName, step.getName()).toPath(),
					OBJECT_MAPPER.writerWithDefaultPrettyPrinter().writeValueAsBytes(step),
					StandardOpenOption.TRUNCATE_EXISTING);
			updateScenarioProperties();
		} catch (IOException e) {
			LOGGER.warn("Cannot persist the step: {}", step.getName());
			LOGGER.warn(e.getMessage(), e);
		}
	}

	private void updateScenarioProperties() {
		String scenarioName = this.zcParams.get(Constants.SCENARIO_NAME);
		Properties properties = null;
		try (BufferedReader reader = Files.newBufferedReader(FileUtils.getScenarioProperties(scenarioName).toPath());) {
			properties = new Properties();
			properties.load(reader);
		} catch (IOException e) {
			LOGGER.warn("Cannot read the scenarioProperties of : {}", scenarioName);
			LOGGER.warn(e.getMessage(), e);
		}

		Map<String, String> scenarioParams = zcParams.entrySet().stream().filter(r -> {
			return r.getKey().startsWith("zc.sc");
		}).collect(Collectors.toMap(e -> {
			return e.getKey();
		}, e -> {
			return e.getValue();
		}));
		properties.putAll(scenarioParams);
		
		try (BufferedWriter writer = Files.newBufferedWriter(FileUtils.getScenarioProperties(scenarioName).toPath(), StandardOpenOption.APPEND)) {
			properties.store(writer, null);
		} catch (IOException e) {
			LOGGER.warn("Cannot persist the scenarioProperties of : {}", scenarioName);
			LOGGER.warn(e.getMessage(), e);
		}

	}

	private static void removeHeaders(Map<String, Object> headers) {
		excludedHeaders.stream().forEach(h -> headers.remove(h));
	}

	/**
	 * @return the request
	 */
	public Request getRequest() {
		return request;
	}

	/**
	 * @param request the request to set
	 */
	public void setRequest(Request request) {
		this.request = request;
	}

	/**
	 * @return the response
	 */
	public Response getResponse() {
		return response;
	}

	/**
	 * @param response the response to set
	 */
	public void setResponse(Response response) {
		this.response = response;
	}

	/**
	 * @return the responseListner
	 */
	public ResponseListner getResponseListner() {
		return responseListner;
	}

	/**
	 * @param responseListner the responseListner to set
	 */
	public void setResponseListner(ResponseListner responseListner) {
		this.responseListner = responseListner;
	}

	/**
	 * @return the requestListner
	 */
	public RequestListner getRequestListner() {
		return requestListner;
	}

	/**
	 * @param requestListner the requestListner to set
	 */
	public void setRequestListner(RequestListner requestListner) {
		this.requestListner = requestListner;
	}
}