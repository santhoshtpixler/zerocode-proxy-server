package org.jsmart.proxy;

import org.eclipse.jetty.client.api.Request;
import org.eclipse.jetty.client.api.Response;
import org.eclipse.jetty.client.api.Result;
import org.eclipse.jetty.client.util.BufferingResponseListener;

public class ResponseListner extends BufferingResponseListener {

	private RequestListner requestListner;

	public ResponseListner() {
		super();
		this.setRequestListner(new RequestListner());
	}

	@Override
	public void onComplete(Result result) {
		if (result.isSucceeded()) {
			Request request = result.getRequest();
			Response response = result.getResponse();
			new StepComposer(request, response, this).compose(); 
		}

	}

	/**
	 * @return the requestListner
	 */
	public RequestListner getRequestListner() {
		return requestListner;
	}

	/**
	 * @param requestListner the requestListner to set
	 */
	private void setRequestListner(RequestListner requestListner) {
		this.requestListner = requestListner;
	}

}
